/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import modelo.Prato;

/**
 *
 * @author Arthur
 */
public class PratoDAO {
    private Connection connection;
    int pratoId;
    String pratoTipo;
    
    public PratoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Prato prato) throws SQLException{
        String sql = "INSERT INTO Prato(pratoTipo) VALUES(?)";
        try{
            try(PreparedStatement stmt = connection.prepareStatement(sql)){
                stmt.setString(1, prato.getPratoTipo());
                stmt.execute();
                stmt.close();
            }
        }
            catch (SQLException u) { 
                throw new RuntimeException(u);
            }    
        }

}
