/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Turma;

/**
 *
 * @author Arthur
 */
public class TurmaDAO {
    private Connection connection;
    int turmaId;
    String turmaData;
    String turmaHorario;
    int Cardapio_cardapioId;
    
    public TurmaDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Turma turma) throws SQLException{
        String sql = "INSERT INTO Turma(turmaData, turmaHorario, Cardapio_cardapioId) VALUES(?, ?, ?)";
        try{
            try(PreparedStatement stmt = connection.prepareStatement(sql)){
                stmt.setString(1, turma.getTurmaData());
                stmt.setString(2, turma.getTurmaHorario());
                stmt.setInt(3, turma.getCardapio_cardapioId());
                stmt.execute();
                stmt.close();
            }
        }
            catch (SQLException u) { 
                throw new RuntimeException(u);
            }    
        }
    
}
