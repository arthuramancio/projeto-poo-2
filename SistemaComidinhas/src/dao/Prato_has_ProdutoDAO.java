/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Prato_has_Produto;

/**
 *
 * @author Arthur
 */
public class Prato_has_ProdutoDAO {
    private Connection connection;

    
    public Prato_has_ProdutoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Prato_has_Produto prato_has_produto) throws SQLException{
        String sql = "INSERT INTO Prato_has_Produto(Prato_pratoId, Produto_produtoId) VALUES(?, ?)";
        try{
            try(PreparedStatement stmt = connection.prepareStatement(sql)){
                stmt.setInt(1, prato_has_produto.getPrato_pratoId());
                stmt.setInt(2, prato_has_produto.getProduto_produtoId());
                stmt.execute();
                stmt.close();
            }
        }
            catch (SQLException u) { 
                throw new RuntimeException(u);
            }    
        }
}
