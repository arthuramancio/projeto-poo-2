/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import modelo.Aluno;

/**
 *
 * @author Arthur
 */
public class AlunoDAO {
    private Connection connection;
    int alunoId;
    String alunoNome;
    String alunoCpf;
    String alunoEndereco;
    char alunoSexo;
    public AlunoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Aluno aluno) throws SQLException{
        String sql = "INSERT INTO aluno(alunoNome, alunoCpf, alunoEndereco, alunoSexo)VALUES(?,?,?,?)";
        try{
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, aluno.getAlunoNome());
            stmt.setString(2, aluno.getAlunoCpf());
            stmt.setString(3, aluno.getAlunoEndereco());
            stmt.setString(4, aluno.getAlunoSexo());
            stmt.execute();
            stmt.close();
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        } 
    }
    
    /*public ArrayList<Aluno> getList() throws Exception{
        String sql = ("select * from aluno");
        try{
           PreparedStatement stmt = connection.prepareStatement(sql);         
           ResultSet rs = stmt.executeQuery(sql);
           ArrayList<Aluno> lista = new ArrayList<Aluno>();
           Aluno pesDoc;
            while(rs.next()){
                pesDoc = new Aluno(rs.getString(alunoNome),Integer.parseInt(rs.getString(alunoCpf)),rs.getString(alunoEndereco),rs.getString(alunoSexo),Integer.parseInt(rs.getString(alunoId)));
                lista.add(pesDoc);
            }
        }catch (SQLException u) { 
            throw new RuntimeException(u);
        }
        
        rs.close(); 
        return lista;
    }*/
    
    
    

}
