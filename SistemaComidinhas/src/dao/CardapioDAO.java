/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Cardapio;

/**
 *
 * @author Arthur
 */
public class CardapioDAO {
    private Connection connection;
    int cardapioId;
    String cardapioDescricao;
    
    public CardapioDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Cardapio cardapio) throws SQLException{
        String sql = "INSERT INTO Cardapio(cardapioDescricao) VALUES(?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, cardapio.getCardapioDescricao());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
}
