/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Turma_has_Aluno;

/**
 *
 * @author Arthur
 */
public class Turma_has_AlunoDAO {
    private Connection connection;
    
    public Turma_has_AlunoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Turma_has_Aluno turma_has_aluno) throws SQLException{
        String sql = "INSERT INTO Turma_has_Aluno(Turma_turmaId, Aluno_alunoId) VALUES(?, ?)";
        try{
            try(PreparedStatement stmt = connection.prepareStatement(sql)){
                stmt.setInt(1, turma_has_aluno.getTurma_turmaId());
                stmt.setInt(2, turma_has_aluno.getAluno_alunoId());
                stmt.execute();
                stmt.close();
            }
        }catch (SQLException u) { 
                throw new RuntimeException(u);
            } 
    }
}
