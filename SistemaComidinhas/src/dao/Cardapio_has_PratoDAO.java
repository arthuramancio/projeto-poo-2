/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Cardapio_has_Prato;

/**
 *
 * @author Arthur
 */
public class Cardapio_has_PratoDAO {
    
    private Connection connection;
    int Cardapio_cardapioId;
    int Prato_pratoId;
    
    public Cardapio_has_PratoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Cardapio_has_Prato cardapio_has_prato) throws SQLException{
        String sql = "INSERT INTO Cardapio_has_Prato(Cardapio_cardapioId, Prato_pratoId) VALUES(?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setInt(1, cardapio_has_prato.getCardapio_cardapioId());
                stmt.setInt(2, cardapio_has_prato.getPrato_pratoId());               
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
}
