/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import modelo.Produto;

/**
 *
 * @author Arthur
 */
public class ProdutoDAO {
    private Connection connection;
    int produtoId;
    String produtoNome;
    float produtoPreco;
    String produtoDataCompra;
    int CategoriaProduto_categoriaprodId;
    
    public ProdutoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Produto produto) throws SQLException{
        String sql = "INSERT INTO Produto(produtoNome, produtoPreco, produtoDataCompra, CategoriaProduto_categoriaprodId) VALUES(?, ?, ?, ?)";
        try{
            try(PreparedStatement stmt = connection.prepareStatement(sql)){
                stmt.setString(1, produto.getProdutoNome());
                stmt.setFloat(2, produto.getProdutoPreco());
                stmt.setString(3, produto.getProdutoDataCompra());
                stmt.setInt(4, produto.getCategoriaProduto_categoriaprodId());
                stmt.execute();
                stmt.close();
            }
        }
            catch (SQLException u) { 
                throw new RuntimeException(u);
            }    
        }
    
    
        
}
