/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Arthur
 */
@Embeddable
public class TurmaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "turmaId")
    private int turmaId;
    @Basic(optional = false)
    @Column(name = "Cardapio_cardapioId")
    private int cardapiocardapioId;

    public TurmaPK() {
    }

    public TurmaPK(int turmaId, int cardapiocardapioId) {
        this.turmaId = turmaId;
        this.cardapiocardapioId = cardapiocardapioId;
    }

    public int getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(int turmaId) {
        this.turmaId = turmaId;
    }

    public int getCardapiocardapioId() {
        return cardapiocardapioId;
    }

    public void setCardapiocardapioId(int cardapiocardapioId) {
        this.cardapiocardapioId = cardapiocardapioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) turmaId;
        hash += (int) cardapiocardapioId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TurmaPK)) {
            return false;
        }
        TurmaPK other = (TurmaPK) object;
        if (this.turmaId != other.turmaId) {
            return false;
        }
        if (this.cardapiocardapioId != other.cardapiocardapioId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.TurmaPK[ turmaId=" + turmaId + ", cardapiocardapioId=" + cardapiocardapioId + " ]";
    }
    
}
