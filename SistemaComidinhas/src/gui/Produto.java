/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "produto", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findByProdutoId", query = "SELECT p FROM Produto p WHERE p.produtoPK.produtoId = :produtoId")
    , @NamedQuery(name = "Produto.findByProdutoNome", query = "SELECT p FROM Produto p WHERE p.produtoNome = :produtoNome")
    , @NamedQuery(name = "Produto.findByProdutoPreco", query = "SELECT p FROM Produto p WHERE p.produtoPreco = :produtoPreco")
    , @NamedQuery(name = "Produto.findByProdutoDataCompra", query = "SELECT p FROM Produto p WHERE p.produtoDataCompra = :produtoDataCompra")
    , @NamedQuery(name = "Produto.findByCategoriaProdutocategoriaprodId", query = "SELECT p FROM Produto p WHERE p.produtoPK.categoriaProdutocategoriaprodId = :categoriaProdutocategoriaprodId")})
public class Produto implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProdutoPK produtoPK;
    @Basic(optional = false)
    @Column(name = "produtoNome")
    private String produtoNome;
    @Basic(optional = false)
    @Column(name = "produtoPreco")
    private float produtoPreco;
    @Basic(optional = false)
    @Column(name = "produtoDataCompra")
    private String produtoDataCompra;

    public Produto() {
    }

    public Produto(ProdutoPK produtoPK) {
        this.produtoPK = produtoPK;
    }

    public Produto(ProdutoPK produtoPK, String produtoNome, float produtoPreco, String produtoDataCompra) {
        this.produtoPK = produtoPK;
        this.produtoNome = produtoNome;
        this.produtoPreco = produtoPreco;
        this.produtoDataCompra = produtoDataCompra;
    }

    public Produto(int produtoId, int categoriaProdutocategoriaprodId) {
        this.produtoPK = new ProdutoPK(produtoId, categoriaProdutocategoriaprodId);
    }

    public ProdutoPK getProdutoPK() {
        return produtoPK;
    }

    public void setProdutoPK(ProdutoPK produtoPK) {
        this.produtoPK = produtoPK;
    }

    public String getProdutoNome() {
        return produtoNome;
    }

    public void setProdutoNome(String produtoNome) {
        String oldProdutoNome = this.produtoNome;
        this.produtoNome = produtoNome;
        changeSupport.firePropertyChange("produtoNome", oldProdutoNome, produtoNome);
    }

    public float getProdutoPreco() {
        return produtoPreco;
    }

    public void setProdutoPreco(float produtoPreco) {
        float oldProdutoPreco = this.produtoPreco;
        this.produtoPreco = produtoPreco;
        changeSupport.firePropertyChange("produtoPreco", oldProdutoPreco, produtoPreco);
    }

    public String getProdutoDataCompra() {
        return produtoDataCompra;
    }

    public void setProdutoDataCompra(String produtoDataCompra) {
        String oldProdutoDataCompra = this.produtoDataCompra;
        this.produtoDataCompra = produtoDataCompra;
        changeSupport.firePropertyChange("produtoDataCompra", oldProdutoDataCompra, produtoDataCompra);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produtoPK != null ? produtoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.produtoPK == null && other.produtoPK != null) || (this.produtoPK != null && !this.produtoPK.equals(other.produtoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Produto[ produtoPK=" + produtoPK + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
