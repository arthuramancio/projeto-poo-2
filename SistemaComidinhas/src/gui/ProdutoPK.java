/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Arthur
 */
@Embeddable
public class ProdutoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "produtoId")
    private int produtoId;
    @Basic(optional = false)
    @Column(name = "CategoriaProduto_categoriaprodId")
    private int categoriaProdutocategoriaprodId;

    public ProdutoPK() {
    }

    public ProdutoPK(int produtoId, int categoriaProdutocategoriaprodId) {
        this.produtoId = produtoId;
        this.categoriaProdutocategoriaprodId = categoriaProdutocategoriaprodId;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    public int getCategoriaProdutocategoriaprodId() {
        return categoriaProdutocategoriaprodId;
    }

    public void setCategoriaProdutocategoriaprodId(int categoriaProdutocategoriaprodId) {
        this.categoriaProdutocategoriaprodId = categoriaProdutocategoriaprodId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) produtoId;
        hash += (int) categoriaProdutocategoriaprodId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutoPK)) {
            return false;
        }
        ProdutoPK other = (ProdutoPK) object;
        if (this.produtoId != other.produtoId) {
            return false;
        }
        if (this.categoriaProdutocategoriaprodId != other.categoriaProdutocategoriaprodId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.ProdutoPK[ produtoId=" + produtoId + ", categoriaProdutocategoriaprodId=" + categoriaProdutocategoriaprodId + " ]";
    }
    
}
