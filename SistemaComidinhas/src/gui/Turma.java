/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "turma", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Turma.findAll", query = "SELECT t FROM Turma t")
    , @NamedQuery(name = "Turma.findByTurmaId", query = "SELECT t FROM Turma t WHERE t.turmaPK.turmaId = :turmaId")
    , @NamedQuery(name = "Turma.findByTurmaData", query = "SELECT t FROM Turma t WHERE t.turmaData = :turmaData")
    , @NamedQuery(name = "Turma.findByTurmaHorario", query = "SELECT t FROM Turma t WHERE t.turmaHorario = :turmaHorario")
    , @NamedQuery(name = "Turma.findByCardapiocardapioId", query = "SELECT t FROM Turma t WHERE t.turmaPK.cardapiocardapioId = :cardapiocardapioId")})
public class Turma implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TurmaPK turmaPK;
    @Basic(optional = false)
    @Column(name = "turmaData")
    private String turmaData;
    @Basic(optional = false)
    @Column(name = "turmaHorario")
    private String turmaHorario;

    public Turma() {
    }

    public Turma(TurmaPK turmaPK) {
        this.turmaPK = turmaPK;
    }

    public Turma(TurmaPK turmaPK, String turmaData, String turmaHorario) {
        this.turmaPK = turmaPK;
        this.turmaData = turmaData;
        this.turmaHorario = turmaHorario;
    }

    public Turma(int turmaId, int cardapiocardapioId) {
        this.turmaPK = new TurmaPK(turmaId, cardapiocardapioId);
    }

    public TurmaPK getTurmaPK() {
        return turmaPK;
    }

    public void setTurmaPK(TurmaPK turmaPK) {
        this.turmaPK = turmaPK;
    }

    public String getTurmaData() {
        return turmaData;
    }

    public void setTurmaData(String turmaData) {
        String oldTurmaData = this.turmaData;
        this.turmaData = turmaData;
        changeSupport.firePropertyChange("turmaData", oldTurmaData, turmaData);
    }

    public String getTurmaHorario() {
        return turmaHorario;
    }

    public void setTurmaHorario(String turmaHorario) {
        String oldTurmaHorario = this.turmaHorario;
        this.turmaHorario = turmaHorario;
        changeSupport.firePropertyChange("turmaHorario", oldTurmaHorario, turmaHorario);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (turmaPK != null ? turmaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Turma)) {
            return false;
        }
        Turma other = (Turma) object;
        if ((this.turmaPK == null && other.turmaPK != null) || (this.turmaPK != null && !this.turmaPK.equals(other.turmaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Turma[ turmaPK=" + turmaPK + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
