/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "cardapio", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Cardapio.findAll", query = "SELECT c FROM Cardapio c")
    , @NamedQuery(name = "Cardapio.findByCardapioId", query = "SELECT c FROM Cardapio c WHERE c.cardapioId = :cardapioId")
    , @NamedQuery(name = "Cardapio.findByCardapioDescricao", query = "SELECT c FROM Cardapio c WHERE c.cardapioDescricao = :cardapioDescricao")})
public class Cardapio implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cardapioId")
    private Integer cardapioId;
    @Basic(optional = false)
    @Column(name = "cardapioDescricao")
    private String cardapioDescricao;

    public Cardapio() {
    }

    public Cardapio(Integer cardapioId) {
        this.cardapioId = cardapioId;
    }

    public Cardapio(Integer cardapioId, String cardapioDescricao) {
        this.cardapioId = cardapioId;
        this.cardapioDescricao = cardapioDescricao;
    }

    public Integer getCardapioId() {
        return cardapioId;
    }

    public void setCardapioId(Integer cardapioId) {
        Integer oldCardapioId = this.cardapioId;
        this.cardapioId = cardapioId;
        changeSupport.firePropertyChange("cardapioId", oldCardapioId, cardapioId);
    }

    public String getCardapioDescricao() {
        return cardapioDescricao;
    }

    public void setCardapioDescricao(String cardapioDescricao) {
        String oldCardapioDescricao = this.cardapioDescricao;
        this.cardapioDescricao = cardapioDescricao;
        changeSupport.firePropertyChange("cardapioDescricao", oldCardapioDescricao, cardapioDescricao);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardapioId != null ? cardapioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cardapio)) {
            return false;
        }
        Cardapio other = (Cardapio) object;
        if ((this.cardapioId == null && other.cardapioId != null) || (this.cardapioId != null && !this.cardapioId.equals(other.cardapioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Cardapio[ cardapioId=" + cardapioId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
