/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "aluno", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Aluno.findAll", query = "SELECT a FROM Aluno a")
    , @NamedQuery(name = "Aluno.findByAlunoId", query = "SELECT a FROM Aluno a WHERE a.alunoId = :alunoId")
    , @NamedQuery(name = "Aluno.findByAlunoNome", query = "SELECT a FROM Aluno a WHERE a.alunoNome = :alunoNome")
    , @NamedQuery(name = "Aluno.findByAlunoCpf", query = "SELECT a FROM Aluno a WHERE a.alunoCpf = :alunoCpf")
    , @NamedQuery(name = "Aluno.findByAlunoEndereco", query = "SELECT a FROM Aluno a WHERE a.alunoEndereco = :alunoEndereco")
    , @NamedQuery(name = "Aluno.findByAlunoSexo", query = "SELECT a FROM Aluno a WHERE a.alunoSexo = :alunoSexo")})
public class Aluno implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "alunoId")
    private Integer alunoId;
    @Basic(optional = false)
    @Column(name = "alunoNome")
    private String alunoNome;
    @Basic(optional = false)
    @Column(name = "alunoCpf")
    private String alunoCpf;
    @Basic(optional = false)
    @Column(name = "alunoEndereco")
    private String alunoEndereco;
    @Basic(optional = false)
    @Column(name = "alunoSexo")
    private String alunoSexo;

    public Aluno() {
    }

    public Aluno(Integer alunoId) {
        this.alunoId = alunoId;
    }

    public Aluno(Integer alunoId, String alunoNome, String alunoCpf, String alunoEndereco, String alunoSexo) {
        this.alunoId = alunoId;
        this.alunoNome = alunoNome;
        this.alunoCpf = alunoCpf;
        this.alunoEndereco = alunoEndereco;
        this.alunoSexo = alunoSexo;
    }

    public Integer getAlunoId() {
        return alunoId;
    }

    public void setAlunoId(Integer alunoId) {
        Integer oldAlunoId = this.alunoId;
        this.alunoId = alunoId;
        changeSupport.firePropertyChange("alunoId", oldAlunoId, alunoId);
    }

    public String getAlunoNome() {
        return alunoNome;
    }

    public void setAlunoNome(String alunoNome) {
        String oldAlunoNome = this.alunoNome;
        this.alunoNome = alunoNome;
        changeSupport.firePropertyChange("alunoNome", oldAlunoNome, alunoNome);
    }

    public String getAlunoCpf() {
        return alunoCpf;
    }

    public void setAlunoCpf(String alunoCpf) {
        String oldAlunoCpf = this.alunoCpf;
        this.alunoCpf = alunoCpf;
        changeSupport.firePropertyChange("alunoCpf", oldAlunoCpf, alunoCpf);
    }

    public String getAlunoEndereco() {
        return alunoEndereco;
    }

    public void setAlunoEndereco(String alunoEndereco) {
        String oldAlunoEndereco = this.alunoEndereco;
        this.alunoEndereco = alunoEndereco;
        changeSupport.firePropertyChange("alunoEndereco", oldAlunoEndereco, alunoEndereco);
    }

    public String getAlunoSexo() {
        return alunoSexo;
    }

    public void setAlunoSexo(String alunoSexo) {
        String oldAlunoSexo = this.alunoSexo;
        this.alunoSexo = alunoSexo;
        changeSupport.firePropertyChange("alunoSexo", oldAlunoSexo, alunoSexo);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alunoId != null ? alunoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aluno)) {
            return false;
        }
        Aluno other = (Aluno) object;
        if ((this.alunoId == null && other.alunoId != null) || (this.alunoId != null && !this.alunoId.equals(other.alunoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Aluno[ alunoId=" + alunoId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
