/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "categoriaproduto", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Categoriaproduto.findAll", query = "SELECT c FROM Categoriaproduto c")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodId", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodId = :categoriaprodId")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodNome", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodNome = :categoriaprodNome")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodDescricao", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodDescricao = :categoriaprodDescricao")})
public class Categoriaproduto implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "categoriaprodId")
    private Integer categoriaprodId;
    @Basic(optional = false)
    @Column(name = "categoriaprodNome")
    private String categoriaprodNome;
    @Basic(optional = false)
    @Column(name = "categoriaprodDescricao")
    private String categoriaprodDescricao;

    public Categoriaproduto() {
    }

    public Categoriaproduto(Integer categoriaprodId) {
        this.categoriaprodId = categoriaprodId;
    }

    public Categoriaproduto(Integer categoriaprodId, String categoriaprodNome, String categoriaprodDescricao) {
        this.categoriaprodId = categoriaprodId;
        this.categoriaprodNome = categoriaprodNome;
        this.categoriaprodDescricao = categoriaprodDescricao;
    }

    public Integer getCategoriaprodId() {
        return categoriaprodId;
    }

    public void setCategoriaprodId(Integer categoriaprodId) {
        Integer oldCategoriaprodId = this.categoriaprodId;
        this.categoriaprodId = categoriaprodId;
        changeSupport.firePropertyChange("categoriaprodId", oldCategoriaprodId, categoriaprodId);
    }

    public String getCategoriaprodNome() {
        return categoriaprodNome;
    }

    public void setCategoriaprodNome(String categoriaprodNome) {
        String oldCategoriaprodNome = this.categoriaprodNome;
        this.categoriaprodNome = categoriaprodNome;
        changeSupport.firePropertyChange("categoriaprodNome", oldCategoriaprodNome, categoriaprodNome);
    }

    public String getCategoriaprodDescricao() {
        return categoriaprodDescricao;
    }

    public void setCategoriaprodDescricao(String categoriaprodDescricao) {
        String oldCategoriaprodDescricao = this.categoriaprodDescricao;
        this.categoriaprodDescricao = categoriaprodDescricao;
        changeSupport.firePropertyChange("categoriaprodDescricao", oldCategoriaprodDescricao, categoriaprodDescricao);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoriaprodId != null ? categoriaprodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriaproduto)) {
            return false;
        }
        Categoriaproduto other = (Categoriaproduto) object;
        if ((this.categoriaprodId == null && other.categoriaprodId != null) || (this.categoriaprodId != null && !this.categoriaprodId.equals(other.categoriaprodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Categoriaproduto[ categoriaprodId=" + categoriaprodId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
