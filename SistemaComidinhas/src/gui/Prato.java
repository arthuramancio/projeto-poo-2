/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "prato", catalog = "dbpoo2", schema = "")
@NamedQueries({
    @NamedQuery(name = "Prato.findAll", query = "SELECT p FROM Prato p")
    , @NamedQuery(name = "Prato.findByPratoId", query = "SELECT p FROM Prato p WHERE p.pratoId = :pratoId")
    , @NamedQuery(name = "Prato.findByPratoTipo", query = "SELECT p FROM Prato p WHERE p.pratoTipo = :pratoTipo")})
public class Prato implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pratoId")
    private Integer pratoId;
    @Basic(optional = false)
    @Column(name = "pratoTipo")
    private String pratoTipo;

    public Prato() {
    }

    public Prato(Integer pratoId) {
        this.pratoId = pratoId;
    }

    public Prato(Integer pratoId, String pratoTipo) {
        this.pratoId = pratoId;
        this.pratoTipo = pratoTipo;
    }

    public Integer getPratoId() {
        return pratoId;
    }

    public void setPratoId(Integer pratoId) {
        Integer oldPratoId = this.pratoId;
        this.pratoId = pratoId;
        changeSupport.firePropertyChange("pratoId", oldPratoId, pratoId);
    }

    public String getPratoTipo() {
        return pratoTipo;
    }

    public void setPratoTipo(String pratoTipo) {
        String oldPratoTipo = this.pratoTipo;
        this.pratoTipo = pratoTipo;
        changeSupport.firePropertyChange("pratoTipo", oldPratoTipo, pratoTipo);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pratoId != null ? pratoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prato)) {
            return false;
        }
        Prato other = (Prato) object;
        if ((this.pratoId == null && other.pratoId != null) || (this.pratoId != null && !this.pratoId.equals(other.pratoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Prato[ pratoId=" + pratoId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
