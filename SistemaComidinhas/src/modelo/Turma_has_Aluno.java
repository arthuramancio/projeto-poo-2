/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Turma_has_Aluno {
    int Turma_turmaId;
    int Aluno_alunoId;

    public int getTurma_turmaId() {
        return Turma_turmaId;
    }

    public void setTurma_turmaId(int Turma_turmaId) {
        this.Turma_turmaId = Turma_turmaId;
    }

    public int getAluno_alunoId() {
        return Aluno_alunoId;
    }

    public void setAluno_alunoId(int Aluno_alunoId) {
        this.Aluno_alunoId = Aluno_alunoId;
    }
    
}
