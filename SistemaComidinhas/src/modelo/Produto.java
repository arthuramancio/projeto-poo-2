/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;



/**
 *
 * @author Arthur
 */
public class Produto {
    int produtoId;
    String produtoNome;
    float produtoPreco;
    String produtoDataCompra;
    int CategoriaProduto_categoriaprodId;



    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    public String getProdutoNome() {
        return produtoNome;
    }

    public void setProdutoNome(String produtoNome) {
        this.produtoNome = produtoNome;
    }

    public float getProdutoPreco() {
        return produtoPreco;
    }

    public void setProdutoPreco(float produtoPreco) {
        this.produtoPreco = produtoPreco;
    }

    public String getProdutoDataCompra() {
        return produtoDataCompra;
    }

    public void setProdutoDataCompra(String produtoDataCompra) {
        this.produtoDataCompra = produtoDataCompra;
    }

    public int getCategoriaProduto_categoriaprodId() {
        return CategoriaProduto_categoriaprodId;
    }

    public void setCategoriaProduto_categoriaprodId(int CategoriaProduto_categoriaprodId) {
        this.CategoriaProduto_categoriaprodId = CategoriaProduto_categoriaprodId;
    }
    
}
