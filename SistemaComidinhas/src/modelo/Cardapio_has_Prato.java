/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Cardapio_has_Prato {
    int Cardapio_cardapioId;
    int Prato_pratoId;

    public int getCardapio_cardapioId() {
        return Cardapio_cardapioId;
    }

    public void setCardapio_cardapioId(int Cardapio_cardapioId) {
        this.Cardapio_cardapioId = Cardapio_cardapioId;
    }

    public int getPrato_pratoId() {
        return Prato_pratoId;
    }

    public void setPrato_pratoId(int Prato_pratoId) {
        this.Prato_pratoId = Prato_pratoId;
    }

    
}
