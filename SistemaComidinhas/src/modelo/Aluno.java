/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Aluno {
    int alunoId;
    String alunoNome;
    String alunoCpf;
    String alunoEndereco;
    String alunoSexo;

    public int getAlunoId() {
        return alunoId;
    }

    public void setAlunoId(int alunoId) {
        this.alunoId = alunoId;
    }

    public String getAlunoNome() {
        return alunoNome;
    }

    public void setAlunoNome(String alunoNome) {
        this.alunoNome = alunoNome;
    }

    public String getAlunoCpf() {
        return alunoCpf;
    }

    public void setAlunoCpf(String alunoCpf) {
        this.alunoCpf = alunoCpf;
    }

    public String getAlunoEndereco() {
        return alunoEndereco;
    }

    public void setAlunoEndereco(String alunoEndereco) {
        this.alunoEndereco = alunoEndereco;
    }

    public String getAlunoSexo() {
        return alunoSexo;
    }

    public void setAlunoSexo(String alunoSexo) {
        this.alunoSexo = alunoSexo;
    }


   
}
