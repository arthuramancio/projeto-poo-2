/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Turma {
    int turmaId;
    String turmaData;
    String turmaHorario;
    int Cardapio_cardapioId;

    public int getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(int turmaId) {
        this.turmaId = turmaId;
    }

    public String getTurmaData() {
        return turmaData;
    }

    public void setTurmaData(String turmaData) {
        this.turmaData = turmaData;
    }

    public String getTurmaHorario() {
        return turmaHorario;
    }

    public void setTurmaHorario(String turmaHorario) {
        this.turmaHorario = turmaHorario;
    }

    public int getCardapio_cardapioId() {
        return Cardapio_cardapioId;
    }

    public void setCardapio_cardapioId(int Cardapio_cardapioId) {
        this.Cardapio_cardapioId = Cardapio_cardapioId;
    }
}
