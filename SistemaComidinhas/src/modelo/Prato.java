/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Prato {
    int pratoId;
    String pratoTipo;


    public int getPratoId() {
        return pratoId;
    }

    public void setPratoId(int pratoId) {
        this.pratoId = pratoId;
    }

    public String getPratoTipo() {
        return pratoTipo;
    }

    public void setPratoTipo(String pratoTipo) {
        this.pratoTipo = pratoTipo;
    }

    @Override
    public String toString() {
        return "Tipo: " + pratoTipo + '}';
    }
    
}
