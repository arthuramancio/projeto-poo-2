/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Cardapio {
    int cardapioId;
    String cardapioDescricao;

    public int getCardapioId() {
        return cardapioId;
    }

    public void setCardapioId(int cardapioId) {
        this.cardapioId = cardapioId;
    }

    public String getCardapioDescricao() {
        return cardapioDescricao;
    }

    public void setCardapioDescricao(String cardapioDescricao) {
        this.cardapioDescricao = cardapioDescricao;
    }

}
