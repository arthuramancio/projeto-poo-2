/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class CategoriaProduto {
    int categoriaprodId;
    String categoriaprodNome;
    String categoriaprodDescricao;

    public int getCategoriaprodId() {
        return categoriaprodId;
    }

    public void setCategoriaprodId(int categoriaprodId) {
        this.categoriaprodId = categoriaprodId;
    }

    public String getCategoriaprodNome() {
        return categoriaprodNome;
    }

    public void setCategoriaprodNome(String categoriaprodNome) {
        this.categoriaprodNome = categoriaprodNome;
    }

    public String getCategoriaprodDescricao() {
        return categoriaprodDescricao;
    }

    public void setCategoriaprodDescricao(String categoriaprodDescricao) {
        this.categoriaprodDescricao = categoriaprodDescricao;
    }
}
