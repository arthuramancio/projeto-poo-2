/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DAOFactory;

import db.daoimplementation.ProfessorDAOImpl;
import db.daointerface.ProfessorDAO;
import db.model.TbProfessor;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author admin
 */
@ManagedBean(name = "profController")
@SessionScoped
public class ProfessorController {

    private String mensagem = "";
    private boolean alunoInserido = false;
    private TbProfessor professor = new TbProfessor();

    public void inserirProfessor() {
        ProfessorDAO p = DAOFactory.getProfessorDAO();
        p.inserirProfessor(this.professor);
        if (professor != null && professor.getNrCpf()!= null) {
            this.setMensagem("PRofessor " + this.professor.getNmProfessor() + ", com o CPF " + this.professor.getNrCpf()+ " inserido com sucesso!");
        }
    }

    public void excluirProfessor(ActionEvent event) {
        String nrCpf = "";
        nrCpf = (String) event.getComponent().getAttributes().get("numeroCpf");
        TbProfessor excluir = new TbProfessor();
        excluir.setNrCpf((nrCpf));
        ProfessorDAO p = DAOFactory.getProfessorDAO();
        if (p.delteProfessor(excluir)) {
            this.setMensagem("Professor " + this.professor.getNmProfessor()+ ", com cpf " + this.professor.getNrCpf()+ " EXCLUÍDO!");
        }
    }

    public void selecionarProfessor(ActionEvent event) {
        String nrCpf= "";
        nrCpf = (String) event.getComponent().getAttributes().get("numeroCpf");
        TbProfessor selecionar = new TbProfessor();
        selecionar.setNrCpf((nrCpf));
        System.out.println(selecionar);
        ProfessorDAO p = DAOFactory.getProfessorDAO();
        TbProfessor retorno = p.selecionarProfessor(selecionar);
        System.out.println(retorno);
        if (retorno != null) {
            this.professor= retorno;
            this.setMensagem("Professor " + this.professor.getNmProfessor()+ ", com CPF" + this.professor.getNrCpf()+ " selecionado!");
        }

    }

    public boolean isAlunoInserido() {
        return alunoInserido;
    }

    public void setAlunoInserido(boolean alunoInserido) {
        this.alunoInserido = alunoInserido;
    }

    public List<TbProfessor> getProfessoresCadastrados() {
        return new ProfessorDAOImpl().selecionarProfessores(null);
    }

    public void atualizarProfessor() {
        ProfessorDAO a = DAOFactory.getProfessorDAO();
        a.modificarProfessor(professor);
        if (professor != null && professor.getNrCpf()!= null) {
            this.setAlunoInserido(true);
            this.setMensagem("Professor " + this.professor.getNmProfessor()+ ", com CPF " + this.professor.getNrCpf()+ " atualizado com sucesso!");
        }
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public TbProfessor getProfessor() {
        return professor;
    }

    public void setProfessor(TbProfessor professor) {
        this.professor = professor;
    }


}
