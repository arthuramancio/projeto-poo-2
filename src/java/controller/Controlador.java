/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DAOFactory;
import db.daoimplementation.AlunoDAOImpl;
import db.daointerface.AlunoDAO;
import db.model.TbAluno;
import java.util.List;
//import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

/**
 *
 * @author admin
 */
@ManagedBean(name = "controlador")
@SessionScoped
public class Controlador {

    private String mensagem = "";
    private boolean alunoInserido = false;
    private TbAluno aluno = new TbAluno();

    public void inserirAluno() {
        AlunoDAO a = DAOFactory.getAlunoDAO();
        a.inserirAluno(aluno);
        if (aluno != null && aluno.getNrMatricula() != null) {
            this.setAlunoInserido(true);
            this.setMensagem("Aluno " + this.aluno.getNmAluno() + ", com matrícula " + this.aluno.getNrMatricula() + " inserido com sucesso!");            
        }
    }

    public void excluirAluno(ActionEvent event) {
        int nrMatricula = 0;
        nrMatricula = (Integer) event.getComponent().getAttributes().get("numeroMatricula");
        TbAluno excluir = new TbAluno();
        excluir.setNrMatricula(Integer.valueOf(nrMatricula));
        AlunoDAO a = DAOFactory.getAlunoDAO();
        if (a.delteAluno(excluir)) {
            this.setMensagem("Aluno " + this.aluno.getNmAluno() + ", com matrícula " + this.aluno.getNrMatricula() + " EXCLUÍDO!");
        }
    }

    public void selecionarAluno(ActionEvent event) {
        int nrMatricula = 0;
        nrMatricula = (Integer) event.getComponent().getAttributes().get("numeroMatricula");
        TbAluno selecionar = new TbAluno();
        selecionar.setNrMatricula(Integer.valueOf(nrMatricula));
        System.out.println(selecionar);
        AlunoDAO a = DAOFactory.getAlunoDAO();
        TbAluno retorno = a.selecionarAluno(selecionar);
        System.out.println(retorno);
        if (retorno != null) {
            this.aluno = retorno;
            this.setMensagem("Aluno " + this.aluno.getNmAluno() + ", com matrícula " + this.aluno.getNrMatricula() + " selecionado!");
        }

    }

    public TbAluno getAluno() {
        return aluno;
    }

    public void setAluno(TbAluno aluno) {
        this.aluno = aluno;
    }

    public boolean isAlunoInserido() {
        return alunoInserido;
    }

    public void setAlunoInserido(boolean alunoInserido) {
        this.alunoInserido = alunoInserido;
    }

    public List<TbAluno> getAlunosCadastrados() {
        return new AlunoDAOImpl().selecionarAlunos(null);
    }

    public void atualizarAluno() {
        AlunoDAO a = DAOFactory.getAlunoDAO();
        a.modificarAluno(aluno);
        if (aluno != null && aluno.getNrMatricula() != null) {
            this.setAlunoInserido(true);
            this.setMensagem("Aluno " + this.aluno.getNmAluno() + ", com matrícula " + this.aluno.getNrMatricula() + " atualizado com sucesso!");
        }
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

}
