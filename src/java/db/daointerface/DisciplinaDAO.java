/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.daointerface;

import db.model.TbDisciplina;
import java.util.List;

/**
 *
 * @author admin
 */
public interface DisciplinaDAO {
    public void delteDisciplina(TbDisciplina a);
    public TbDisciplina selecionarDisciplina(TbDisciplina a);
    public List<TbDisciplina> selecionarDisciplinas(TbDisciplina a);
    public TbDisciplina inserirDisciplina(TbDisciplina a);
    public boolean modificarDisciplina(TbDisciplina a);
    
}
