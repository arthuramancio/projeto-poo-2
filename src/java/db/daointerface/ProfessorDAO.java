/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.daointerface;

import db.model.TbProfessor;
import java.util.List;

/**
 *
 * @author admin
 */
public interface ProfessorDAO {
    public boolean delteProfessor(TbProfessor a);
    public TbProfessor selecionarProfessor(TbProfessor a);
    public List<TbProfessor> selecionarProfessores(TbProfessor a);
    public TbProfessor inserirProfessor(TbProfessor a);
    public boolean modificarProfessor(TbProfessor a);
    
}
