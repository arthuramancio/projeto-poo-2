/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import db.daoimplementation.AlunoDAOImpl;
import db.daoimplementation.ProfessorDAOImpl;
import db.daointerface.AlunoDAO;
import db.daointerface.ProfessorDAO;
import java.sql.Connection;

/**
 *
 * @author admin
 */
public class DAOFactory {

    public static Connection getDatabaseConnection() {
        return PGSQLConnection.getDatabaseConnection();
    }
    
    public static AlunoDAO getAlunoDAO(){
        return new AlunoDAOImpl();
    }
    
    public static ProfessorDAO getProfessorDAO(){
        return new ProfessorDAOImpl();
    }
}
