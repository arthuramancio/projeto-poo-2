/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_aluno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbAluno.findAll", query = "SELECT t FROM TbAluno t")
    , @NamedQuery(name = "TbAluno.findByNrMatricula", query = "SELECT t FROM TbAluno t WHERE t.nrMatricula = :nrMatricula")
    , @NamedQuery(name = "TbAluno.findByNrCpf", query = "SELECT t FROM TbAluno t WHERE t.nrCpf = :nrCpf")
    , @NamedQuery(name = "TbAluno.findByNmAluno", query = "SELECT t FROM TbAluno t WHERE t.nmAluno = :nmAluno")
    , @NamedQuery(name = "TbAluno.findByNmEmail", query = "SELECT t FROM TbAluno t WHERE t.nmEmail = :nmEmail")
    , @NamedQuery(name = "TbAluno.findByNrTelefone", query = "SELECT t FROM TbAluno t WHERE t.nrTelefone = :nrTelefone")})
public class TbAluno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nr_matricula")
    private Integer nrMatricula;
    @Basic(optional = false)
    @Column(name = "nr_cpf")
    private String nrCpf;
    @Column(name = "nm_aluno")
    private String nmAluno;
    @Column(name = "nm_email")
    private String nmEmail;
    @Column(name = "nr_telefone")
    private String nrTelefone;

    public TbAluno() {
    }

    public TbAluno(Integer nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public TbAluno(Integer nrMatricula, String nrCpf) {
        this.nrMatricula = nrMatricula;
        this.nrCpf = nrCpf;
    }

    public Integer getNrMatricula() {
        return nrMatricula;
    }

    public void setNrMatricula(Integer nrMatricula) {
        this.nrMatricula = nrMatricula;
    }

    public String getNrCpf() {
        return nrCpf;
    }

    public void setNrCpf(String nrCpf) {
        this.nrCpf = nrCpf;
    }

    public String getNmAluno() {
        return nmAluno;
    }

    public void setNmAluno(String nmAluno) {
        this.nmAluno = nmAluno;
    }

    public String getNmEmail() {
        return nmEmail;
    }

    public void setNmEmail(String nmEmail) {
        this.nmEmail = nmEmail;
    }

    public String getNrTelefone() {
        return nrTelefone;
    }

    public void setNrTelefone(String nrTelefone) {
        this.nrTelefone = nrTelefone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nrMatricula != null ? nrMatricula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbAluno)) {
            return false;
        }
        TbAluno other = (TbAluno) object;
        if ((this.nrMatricula == null && other.nrMatricula != null) || (this.nrMatricula != null && !this.nrMatricula.equals(other.nrMatricula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.model.TbAluno[ nrMatricula=" + nrMatricula + " ]";
    }
    
}
