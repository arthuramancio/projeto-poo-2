/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_professor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbProfessor.findAll", query = "SELECT t FROM TbProfessor t")
    , @NamedQuery(name = "TbProfessor.findByNrCpf", query = "SELECT t FROM TbProfessor t WHERE t.nrCpf = :nrCpf")
    , @NamedQuery(name = "TbProfessor.findByNmProfessor", query = "SELECT t FROM TbProfessor t WHERE t.nmProfessor = :nmProfessor")
    , @NamedQuery(name = "TbProfessor.findByNmEmail", query = "SELECT t FROM TbProfessor t WHERE t.nmEmail = :nmEmail")
    , @NamedQuery(name = "TbProfessor.findByNrTelefone", query = "SELECT t FROM TbProfessor t WHERE t.nrTelefone = :nrTelefone")})
public class TbProfessor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "nr_cpf")
    private String nrCpf;
    @Column(name = "nm_professor")
    private String nmProfessor;
    @Column(name = "nm_email")
    private String nmEmail;
    @Column(name = "nr_telefone")
    private String nrTelefone;

    public TbProfessor() {
    }

    public TbProfessor(String nrCpf, String nmProfessor, String nmEmail, String nrTelefone) {
        this.nrCpf = nrCpf;
        this.nmProfessor = nmProfessor;
        this.nmEmail = nmEmail;
        this.nrTelefone = nrTelefone;
    }

    public TbProfessor(String nrCpf) {
        this.nrCpf = nrCpf;
    }

    public String getNrCpf() {
        return nrCpf;
    }

    public void setNrCpf(String nrCpf) {
        this.nrCpf = nrCpf;
    }

    public String getNmProfessor() {
        return nmProfessor;
    }

    public void setNmProfessor(String nmProfessor) {
        this.nmProfessor = nmProfessor;
    }

    public String getNmEmail() {
        return nmEmail;
    }

    public void setNmEmail(String nmEmail) {
        this.nmEmail = nmEmail;
    }

    public String getNrTelefone() {
        return nrTelefone;
    }

    public void setNrTelefone(String nrTelefone) {
        this.nrTelefone = nrTelefone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nrCpf != null ? nrCpf.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbProfessor)) {
            return false;
        }
        TbProfessor other = (TbProfessor) object;
        if ((this.nrCpf == null && other.nrCpf != null) || (this.nrCpf != null && !this.nrCpf.equals(other.nrCpf))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.model.TbProfessor[ nrCpf=" + nrCpf + " nome=" + nmProfessor + " email=" + nmEmail + " ]";
    }
    
}
