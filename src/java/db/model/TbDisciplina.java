/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "tb_disciplina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDisciplina.findAll", query = "SELECT t FROM TbDisciplina t")
    , @NamedQuery(name = "TbDisciplina.findByIdDisciplina", query = "SELECT t FROM TbDisciplina t WHERE t.idDisciplina = :idDisciplina")
    , @NamedQuery(name = "TbDisciplina.findByNmDisciplina", query = "SELECT t FROM TbDisciplina t WHERE t.nmDisciplina = :nmDisciplina")
    , @NamedQuery(name = "TbDisciplina.findByNmEmenta", query = "SELECT t FROM TbDisciplina t WHERE t.nmEmenta = :nmEmenta")
    , @NamedQuery(name = "TbDisciplina.findByNrCh", query = "SELECT t FROM TbDisciplina t WHERE t.nrCh = :nrCh")})
public class TbDisciplina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_disciplina")
    private Integer idDisciplina;
    @Column(name = "nm_disciplina")
    private String nmDisciplina;
    @Column(name = "nm_ementa")
    private String nmEmenta;
    @Column(name = "nr_ch")
    private Integer nrCh;

    public TbDisciplina() {
    }

    public TbDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getNmDisciplina() {
        return nmDisciplina;
    }

    public void setNmDisciplina(String nmDisciplina) {
        this.nmDisciplina = nmDisciplina;
    }

    public String getNmEmenta() {
        return nmEmenta;
    }

    public void setNmEmenta(String nmEmenta) {
        this.nmEmenta = nmEmenta;
    }

    public Integer getNrCh() {
        return nrCh;
    }

    public void setNrCh(Integer nrCh) {
        this.nrCh = nrCh;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDisciplina != null ? idDisciplina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDisciplina)) {
            return false;
        }
        TbDisciplina other = (TbDisciplina) object;
        if ((this.idDisciplina == null && other.idDisciplina != null) || (this.idDisciplina != null && !this.idDisciplina.equals(other.idDisciplina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.model.TbDisciplina[ idDisciplina=" + idDisciplina + " ]";
    }
    
}
