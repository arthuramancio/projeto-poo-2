/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.daoimplementation;

import db.DAOFactory;
import db.daointerface.ProfessorDAO;
import db.model.TbAluno;
import db.model.TbProfessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class ProfessorDAOImpl implements ProfessorDAO {

    @Override
    public boolean delteProfessor(TbProfessor a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM sc_universidade.tb_professor WHERE nr_cpf = ?");
            ps.setString(1, a.getNrCpf());
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public TbProfessor selecionarProfessor(TbProfessor a) {
        TbProfessor professorSelecionado = new TbProfessor();
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            //PENDÊNCIA : fazer um filtro mais elaborado com os dados de Aluno
            String strSQL = "SELECT * FROM sc_universidade.tb_professor WHERE nr_cpf = ?";
            PreparedStatement ps = con.prepareStatement(strSQL);
            ps.setString(1, a.getNrCpf());
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {

                professorSelecionado.setNrCpf(rs.getString("nr_cpf"));
                professorSelecionado.setNmProfessor(rs.getString("nm_professor"));
                professorSelecionado.setNmEmail(rs.getString("nm_email"));
                professorSelecionado.setNrTelefone(rs.getString("nr_telefone"));
            }
            ps.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ProfessorDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(professorSelecionado);
        return professorSelecionado;
    }

    @Override
    public List<TbProfessor> selecionarProfessores(TbProfessor a) {
        try {
            List<TbProfessor> listaProfessores  = new ArrayList<TbProfessor>();
            Connection con = DAOFactory.getDatabaseConnection();
            //PENDÊNCIA : fazer um filtro mais elaborado com os dados de Aluno
            String strSQL = "SELECT * FROM sc_universidade.tb_professor ";
            if (a != null && a.getNmProfessor() != null) {
                strSQL += "WHERE nm_professor ilike '%?%' ";
            }

            PreparedStatement ps = con.prepareStatement(strSQL);
            if (a != null && a.getNmProfessor() != null) {
                ps.setString(1, a.getNmProfessor());
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TbProfessor alunoSelecionado = new TbProfessor();
                alunoSelecionado.setNrCpf(rs.getString("nr_cpf"));
                alunoSelecionado.setNmProfessor(rs.getString("nm_professor"));
                alunoSelecionado.setNmEmail(rs.getString("nm_email"));
                alunoSelecionado.setNrTelefone(rs.getString("nr_telefone"));
                listaProfessores.add(alunoSelecionado);
            }
            ps.close();
            con.close();
            return listaProfessores;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public TbProfessor inserirProfessor(TbProfessor prof) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO sc_universidade.tb_professor (nm_professor, nm_email, nr_cpf, nr_telefone) "
                            + "values (?, ?, ?, ?) ");
            //ps.setInt(1, a.getNrMatricula());
            ps.setString(1, prof.getNmProfessor());
            ps.setString(2, prof.getNmEmail());
            ps.setString(3, prof.getNrCpf());
            ps.setString(4, prof.getNrTelefone());
            ResultSet rs = ps.executeQuery();
            ps.close();
            con.close();
            return prof;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean modificarProfessor(TbProfessor a) {
        try {
            Connection con = DAOFactory.getDatabaseConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE "
                    + "sc_universidade.tb_professor set nm_professor =? , "
                    + "nm_email = ?, nr_telefone = ? WHERE nr_cpf = ? ");
            ps.setString(4, a.getNrCpf());
            ps.setString(1, a.getNmProfessor());
            ps.setString(2, a.getNmEmail());
            ps.setString(3, a.getNrTelefone());
            ps.execute();
            ps.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(AlunoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
